#include "../include/sim.hpp"
using namespace vulcanized;

sim::device_container::device_container(VkSurfaceKHR &surface, VkPhysicalDevice &dev) : physical(dev) {
    vkGetPhysicalDeviceProperties(dev, &properties);
    vkGetPhysicalDeviceFeatures(dev, &features);

    char string[4096]{0};
    std::snprintf(string, 4095, "sim::device_container - we have a %s", properties.deviceName);
    console::cprintf(console::level::info, string);

    std::uint32_t queue_family_count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(dev, &queue_family_count, nullptr);
    std::vector<VkQueueFamilyProperties> queue_families{};
    queue_families.resize(queue_family_count);
    vkGetPhysicalDeviceQueueFamilyProperties(dev, &queue_family_count, queue_families.data());
    std::uint32_t i = 0;
    VkBool32 present = false;
    for(const auto &queue : queue_families) {
        if((queue.queueFlags & VK_QUEUE_GRAPHICS_BIT) && graphics_queue == -1) {
            graphics_queue = i;

            char string2[4096]{0};
            std::snprintf(string2, 4095, "sim::device_container - graphics queue is %u", graphics_queue);
            console::cprintf(console::level::info, string2);
        }
        present = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(dev, i, surface, &present);
        if(present && presentation_queue == -1) {
            presentation_queue = i;

            char string2[4096]{0};
            std::snprintf(string2, 4095, "sim::device_container - present queue is %u", presentation_queue);
            console::cprintf(console::level::info, string2);
        }
        i++;
    }
    assert(graphics_queue != -1);
    assert(presentation_queue != -1);
}

sim::feature_mask sim::start(container &c, feature_masks &&what) {
    switch (what)
    {
        case sim::feature_masks::sdl2: {
            console::cprintf(console::level::info, "sim::start - Loading SDL2 Layer");
            c.mask |= static_cast<sim::feature_mask>(sim::feature_masks::sdl2); // set fail bit
            assert(SDL_Init(SDL_INIT_EVERYTHING) == 0);
            c.window = nullptr;
            c.window = SDL_CreateWindow("Vulcanized", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, c.w, c.h, SDL_WINDOW_VULKAN);
            assert(c.window != nullptr);
            c.mask ^= static_cast<sim::feature_mask>(sim::feature_masks::sdl2); // set success bit
            break;
        }
        case sim::feature_masks::instance: {
            console::cprintf(console::level::info, "sim::start - Loading VkInstance Layer");
            c.mask |= static_cast<sim::feature_mask>(sim::feature_masks::instance); // set fail bit
            // grab extensions for instance
            assert(SDL_Vulkan_GetInstanceExtensions(c.window, &c.extension_count, nullptr) == SDL_TRUE);
            c.extension_names.resize(c.extension_count);
            assert(SDL_Vulkan_GetInstanceExtensions(c.window, &c.extension_count, c.extension_names.data()) == SDL_TRUE);

            // application information
            c.app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
            c.app_info.pApplicationName = "No Name";
            c.app_info.applicationVersion = VK_MAKE_VERSION(0,1,0);
            c.app_info.pEngineName = "Vulcanized";
            c.app_info.engineVersion = VK_MAKE_VERSION(0,1,0);
            c.app_info.apiVersion = VK_API_VERSION_1_1;

            // instance creation information
            c.instance_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
            c.instance_info.pApplicationInfo = &c.app_info;
            c.instance_info.enabledExtensionCount = c.extension_names.size();
            c.instance_info.ppEnabledExtensionNames = c.extension_names.data();
            c.instance_info.enabledLayerCount = 0;

            assert(vkCreateInstance(&c.instance_info, nullptr, &c.instance) == VK_SUCCESS);

            c.mask ^= static_cast<sim::feature_mask>(sim::feature_masks::instance); // set success bit
            break;
        }
        case sim::feature_masks::surface: {
            console::cprintf(console::level::info, "sim::start - Loading VkSurfaceKHR Layer");
            c.mask |= static_cast<sim::feature_mask>(sim::feature_masks::surface); // set fail bit

            assert(SDL_Vulkan_CreateSurface(c.window, c.instance, &c.surface) == SDL_TRUE);

            c.mask ^= static_cast<sim::feature_mask>(sim::feature_masks::surface); // set success bit
            break;
        }
        case sim::feature_masks::physical: {
            console::cprintf(console::level::info, "sim::start - Loading VkPhysicalDevice Layer");
            c.mask |= static_cast<sim::feature_mask>(sim::feature_masks::physical); // set fail bit
            assert(vkEnumeratePhysicalDevices(c.instance, &c.physical_count, nullptr) == VK_SUCCESS);
            assert(c.physical_count > 0);

            std::vector<VkPhysicalDevice> physicals{};
            physicals.resize(c.physical_count);

            assert(vkEnumeratePhysicalDevices(c.instance, &c.physical_count, physicals.data()) == VK_SUCCESS);
            
            for(auto &dev : physicals) {
                c.conts.emplace_back(c.surface, dev);
            }
            c.mask ^= static_cast<sim::feature_mask>(sim::feature_masks::physical); // set success bit
            break;
        }
        case sim::feature_masks::logical: {
            console::cprintf(console::level::info, "sim::start - Loading VkDevice Layer");
            c.mask |= static_cast<sim::feature_mask>(sim::feature_masks::logical); // set fail bit

            assert(SDL_Vulkan_CreateSurface(c.window, c.instance, &c.surface) == SDL_TRUE);

            c.mask ^= static_cast<sim::feature_mask>(sim::feature_masks::logical); // set success bit
            break;
        }
    }
    return c.mask;
}

void sim::end(container &&c) {
    vkDestroySurfaceKHR(c.instance, c.surface, nullptr);
    vkDestroyInstance(c.instance, nullptr);
    SDL_DestroyWindow(c.window);
    SDL_Quit();
}