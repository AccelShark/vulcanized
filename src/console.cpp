#include "../include/console.hpp"
using namespace vulcanized;

#ifdef NDEBUG
constexpr auto minimum_debug = console::level::info;
#else
constexpr auto minimum_debug = console::level::trace;
#endif

// ===========================================================================
// ConsoleRealm: Special set, stores IDs
// ===========================================================================
console::realm::realm() : console::object(0, 0) {
    char string[4096]{0};
    std::snprintf(string, 4095, "create ConsoleRealm, realm = %p", this);
    console::cprintf(console::level::trace, string);
}

const std::unique_ptr<console::object> &console::realm::create(const console::otype otype) {
    _max_idx++;

    char string[4096]{0};
    std::snprintf(string, 4095, "create ConsoleObject, realm = %p, idx = %Iu", this, _max_idx);
    console::cprintf(console::level::trace, string);

    _data.emplace(_max_idx, new console::object(_max_idx, _idx));
    return _data.at(_max_idx);
}

void console::realm::remove(const console::id idx) {
    if(idx == 0) {
        console::cprintf(console::level::warning, "console::realm::remove - can't remove a ConsoleRealm using its own ID");
        return;
    }

    char string[4096]{0};
    std::snprintf(string, 4095, "remove ConsoleObject, realm = %p, idx = %Iu", this, idx);
    console::cprintf(console::level::trace, string);

    // First delete gracefully.
    _data.at(idx).reset(nullptr);

    // Then purge.
    _data.erase(idx);
}

// ===========================================================================
// ConsoleObject: Object with IDs
// ===========================================================================
console::object::object(const console::id idx, const console::id parent) : _idx(idx), _parent(parent) {
    char string[4096]{0};
    std::snprintf(string, 4095, "create ConsoleObject realmlessly, idx = %Iu", idx);
    console::cprintf(console::level::trace, string);
}

console::id console::object::get_id() {
    return _idx;
}

console::otype console::object::get_type() {
    return _otype;
}

// ===========================================================================
// cprintf: LP0 on fire!
// ===========================================================================
void console::cprintf(const console::level level, const char *string) {
    if (static_cast<std::uint_fast8_t>(level) >= static_cast<std::uint_fast8_t>(minimum_debug)) {
        auto time = std::chrono::system_clock::now();
        auto c_representable_time = std::chrono::system_clock::to_time_t(time);

        auto time_msecs = std::chrono::duration_cast<std::chrono::milliseconds>(time.time_since_epoch());
        
        char representable_time[32]{0};

        std::strftime(representable_time, sizeof(representable_time), "%T", std::localtime(&c_representable_time));
        switch (level) {
            case console::level::trace:
                std::fprintf(stderr, "(%s.%.3lu) [TRACE] %s\n", representable_time, static_cast<unsigned long>(time_msecs.count()) % 1000, string);
                break;
            case console::level::debug:
                std::fprintf(stderr, "(%s.%.3lu) [DEBUG] %s\n", representable_time, static_cast<unsigned long>(time_msecs.count()) % 1000, string);
                break;
            case console::level::info:
                std::fprintf(stderr, "(%s.%.3lu) [INFO] %s\n", representable_time, static_cast<unsigned long>(time_msecs.count()) % 1000, string);
                break;
            case console::level::warning:
                std::fprintf(stderr, "(%s.%.3lu) [WARNING] %s\n", representable_time, static_cast<unsigned long>(time_msecs.count()) % 1000,
                             string);
                break;
            case console::level::error:
                std::fprintf(stderr, "(%s.%.3lu) [ERROR] %s\n", representable_time, static_cast<unsigned long>(time_msecs.count()) % 1000, string);
                break;
            case console::level::critical:
                std::fprintf(stderr, "(%s.%.3lu) [CRITICAL] %s\n", representable_time, static_cast<unsigned long>(time_msecs.count()) % 1000,
                             string);
                break;
        }
    }

}