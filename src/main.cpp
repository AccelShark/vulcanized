#include "../include/sim.hpp"

int main(int argc, char *argv[]) {
    std::printf("Vulcanized Game Engine, handing off to con-space...\n");
    vulcanized::console::cprintf(vulcanized::console::level::info, "Hello World.");
    // auto r = std::make_unique<vulcanized::console::realm>();
    // auto &&robj = r->create(vulcanized::console::otype::ConsoleObject);
    // r->remove(1);
    vulcanized::sim::container cont{};
    cont.w = 1920;
    cont.h = 1080;
    assert(!vulcanized::sim::start(cont, vulcanized::sim::feature_masks::sdl2));
    assert(!vulcanized::sim::start(cont, vulcanized::sim::feature_masks::instance));
    assert(!vulcanized::sim::start(cont, vulcanized::sim::feature_masks::surface));
    assert(!vulcanized::sim::start(cont, vulcanized::sim::feature_masks::physical));
    SDL_Delay(5000);
    vulcanized::sim::end(std::move(cont));
    return EXIT_SUCCESS;
}