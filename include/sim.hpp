#pragma once
#include "main.hpp"
#include "console.hpp"

namespace vulcanized {
    namespace sim {
        using feature_mask = std::uint64_t;
        struct device_container {
            VkPhysicalDevice physical;
            VkPhysicalDeviceProperties properties;
            VkPhysicalDeviceFeatures features;
            std::uint32_t graphics_queue = -1;
            std::uint32_t presentation_queue = -1;
            device_container(VkSurfaceKHR &, VkPhysicalDevice &);
        };
        struct container {
            std::uint32_t w;
            std::uint32_t h;
            feature_mask mask = 0;
            std::uint32_t extension_count;
            SDL_Window *window;
            std::vector<const char *> extension_names{};
            std::vector<const char *> layer_names{};
            std::uint32_t physical_count = 0;

            // Any and all Vulkan objects live here.
            VkInstance instance;
            VkSurfaceKHR surface;
            VkApplicationInfo app_info{};
            VkInstanceCreateInfo instance_info{};
            std::vector<device_container> conts{};
            std::uint32_t curr_container = -1;
            VkDeviceQueueCreateInfo graphics_info{};
            VkDeviceQueueCreateInfo present_info{};
        };
        enum class feature_masks : feature_mask {
            success = 0,
            sdl2 = 1 << 0,
            instance = 1 << 1,
            surface = 1 << 2,
            physical = 1 << 3,
            logical = 1 << 4,
            extensions = 1 << 5,
            swap = 1 << 6,
            image_views = 1 << 7,
        };


        feature_mask start(container &, feature_masks &&);
        void end(container &&);
    }
}