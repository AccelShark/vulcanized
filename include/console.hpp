#pragma once
#include "main.hpp"

namespace vulcanized {
    namespace console {
        using id = std::uint64_t;

        enum class otype {
            ConsoleRealm,
            ConsoleObject,
            SimObject
        };

        class object {
            constexpr static otype _otype{otype::ConsoleObject};
        protected:
            id _idx;
            id _parent;
        public:
            explicit object(const id, const id);
            id get_id();
            static otype get_type();
        };

        class realm : public object {
            constexpr static otype _otype{otype::ConsoleRealm};
        protected:
            id _max_idx = 0;
            std::unordered_map<id, std::unique_ptr<object>> _data;
        public:
            // Create a ConsoleRealm
            explicit realm();

            // Create a ConsoleObject in a ConsoleRealm
            const std::unique_ptr<console::object> &create(const console::otype);

            // Remove a ConsoleObject in a ConsoleRealm
            void remove(const console::id);
        };

        enum class level : std::uint_fast8_t {
            trace = 0x01,
            debug = 0x02,
            info = 0x10,
            warning = 0x20,
            error = 0x40,
            critical = 0x80,
        };

        void cprintf(const level, const char *);
    }
}