cmake_minimum_required(VERSION 3.15)
project(Vulcanized VERSION 0.1.0 LANGUAGES CXX)

find_package(SDL2 REQUIRED)
find_package(Vulkan REQUIRED)
find_package(Threads REQUIRED)
include_directories(${Vulkan_INCLUDE_DIRS} ${SDL2_DIR}/../include/)
add_executable(Vulcanized include/main.hpp include/sim.hpp include/console.hpp src/console.cpp src/sim.cpp src/main.cpp)
target_link_libraries(Vulcanized ${Vulkan_LIBRARIES} ${SDL2_DIR}/../lib/SDL2d.lib ${SDL2_DIR}/../lib/SDL2maind.lib)